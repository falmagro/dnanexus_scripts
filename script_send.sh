#!/bin/bash
#
# Example: ./script_send.sh subj.txt 2 GPU 1 low
#    subj.txt is a txt file with a list of subjects to process
#    The format of the subject IDs in that file is $VISIT$ID (8 characters)
#
# Example: ./script_send.sh ${ID} 2 GPU 1 low
#    ${ID} is a string with the ID of the subject to process
#    The format of the subject IDs in that file is $VISIT$ID (8 characters)
#
# Note: Change FBP_TESTS to the appropriate DNANexus directory
#

set -x
set -e

file="";
ID="";
parallel="1"

DNADirectory="FBP_TESTS"
block="1";
procc="CPU";
num_cores="1"
priority="high"

# Checking valid inputs
if [ "$1" == "" ] ; then
   echo "ERROR: No input included"
   exit 1;
fi
if [ ! -f $1 ] ; then
   ID="$1"
else
   file="$1"
fi

if [ ! "$2" == "" ] ; then
   block="$2"
fi

if [ ! "$3" == "" ] ; then
   procc="$3"
fi

if [ ! "$4" == "" ] ; then
   num_cores="$4"
fi

# Checking priority
if [ "$5" == "" ] ; then
   priority="high"
elif [ "$5" == "low" ] ; then
   priority="low"
elif [ "$5" == "normal" ] ; then
   priority="normal"
else
   priority="high"
fi

# Check block
if [ "${block}" -lt "1" ] ; then
  echo "ERROR: Executing block must be between 1 and 6"
  exit 1;
elif [ "${block}" -gt "6" ] ; then
  echo "ERROR: Executing block must be between 1 and 6"
  exit 1;
fi

# Decide on the time limit depending on block, number of subjects and number of cores
num_subjects="1";
if [ ! "${file}" == "" ] ; then
   num_subjects=`cat ${file} |wc -l`
fi

if [ "${block}" == "1" ] ; then
   num_h=`echo "($num_subjects * 6) / ${num_cores}" |bc`
   ml="${num_h}h"
elif [ "${block}" == "2" ] ; then
   num_h=`echo "($num_subjects)" |bc`
   ml="${num_h}h"
elif [ "${block}" == "3" ] ; then
   num_h=`echo "($num_subjects * 2) / ${num_cores}" |bc`
   ml="${num_h}h"
elif [ "${block}" == "4" ] ; then
   num_h=`echo "($num_subjects * 2)" |bc`
   ml="${num_h}h"
elif [ "${block}" == "5" ] ; then
   num_h=`echo "($num_subjects * 24) / ${num_cores}" |bc`
   ml="${num_h}h"
else
   num_h=`echo "($num_subjects * 24) / ${num_cores}" |bc`
   ml="${num_h}h"
fi


# Decide on the machine to use depending on CPU/GPU
if [ "$procc" == "CPU"  ]; then
  machine="mem3_ssd1_v2_x8";
elif [ "$procc" == "GPU"  ]; then
  machine="mem2_ssd1_gpu_x16";
elif [ "$procc" == "CPU2"  ]; then
  machine="mem3_hdd1_v2_x8";
else
  echo "ERROR: Third parameter ($2) can only be CPU, GPU, or CPU2"
  exit 1;
fi

# Upload a file with the subjects to process
nam=`date +"%m-%d-%y_%H-%M-%S.%s"`
tmp=`tmpnam`

if [ "$file" == "" ] ; then
   echo $ID > $tmp
else
   cp $file $tmp
fi

# Upload the file with the IDs to process
dx upload --path $DNADirectory:subjects/s_$nam.txt $tmp
dx rm     -rf    $DNADirectory:scripts/*
dx upload --path $DNADirectory:scripts/ script_proc.sh script_proc_subject.sh killme


# Create the specified machine
jID=`dx run cloud_workstation -y --allow-ssh --instance-type $machine \
   --priority ${priority} --input max_session_length=$ml | grep "Job ID" | awk '{print $3}'`
echo "@SIGNAL@ $jID"

# Send (via ssh) the specified command to the machine
dx ssh $jID -f "nohup /bin/bash -l -c 'dx download $DNADirectory:/scripts/* ; dx download $DNADirectory:subjects/s_$nam.txt ; mv s_$nam.txt subjects.txt ; chmod 755 script_*.sh killme subjects.txt; nohup ./script_proc.sh ${block} ${procc} ${num_cores} &> log_${procc}_${block}_${nam}.txt '" &
