#!/bin/bash

fileExists() {
  # The next command will have exit status 0 (no error) if the file exists
  dx ls "$1" &> /dev/null;

  # Check the exit status of last command
  if [ "$?" == "0" ] ; then
    return 0;
  else
    return 1;
  fi
}

downloadAndMove(){
  modality=`echo "$1" | sed 's|_| |g'`;
  number="$2"
  file="$DNADirectory:/Bulk/Brain MRI/$modality/$START/${ID}_202${number}_${VISIT}_0.zip"

  if fileExists "$file" ; then
    mkdir -p $HOME/data/$ID/DICOM/${number}
    dx download "$file"
    if [ -f ${ID}_202${number}_${VISIT}_0.zip ] ; then
         mv ${ID}_202${number}_${VISIT}_0.zip $HOME/data/$ID/DICOM/${number}/
    fi
  fi
}

export SUBJECT="$1";
export block="$2";
export procc="$3"
export dckr_name="$4";
export DNADirectory="$5"

cd $HOME/data/ ;

ID=${SUBJECT:1:10}
VISIT=${SUBJECT:0:1}
START=${SUBJECT:1:2}

if [ -d $HOME/data/$ID ] ; then
   rm -rf $HOME/data/$ID ;
fi

if [ "$block" == "1" ] ; then

  downloadAndMove "T1"       "16";
  downloadAndMove "tfMRI"    "17";
  downloadAndMove "dMRI"     "18";
  downloadAndMove "SWI"      "19";
  downloadAndMove "T2_FLAIR" "20";
  downloadAndMove "rfMRI"    "25";
  downloadAndMove "ASL"      "66";

  cd ..;

  date;

  JID=`sudo docker run --cpus="1" -e OMP_NUM_THREADS="1" --rm -v $HOME/data/$ID:/ukbdata/$ID ${dckr_name} /fbp/bb_pipeline_v_2.5/script_first.sh $ID $VISIT`
else

   for (( i=1 ; i < $block ; i++ )) ; do
     if fileExists $DNADirectory:/outputs/*_${i}_${SUBJECT}.zip ; then
       dx download $DNADirectory:/outputs/*_${i}_${SUBJECT}.zip --overwrite
       unzip *_${i}_${SUBJECT}.zip ;
       rm -r *_${i}_${SUBJECT}.zip ;
     fi
   done

   cp -r ${ID} ${ID}_bckp
   cd ..;

   date;

   JID=`sudo docker run --cpus="1" -e OMP_NUM_THREADS="1" --rm -v $HOME/data/$ID:/ukbdata/$ID ${dckr_name} /fbp/bb_pipeline_v_2.5/script_generic.sh $ID $VISIT $block `
fi

exitCode=`sudo docker container wait $JID`

rm r_${SUBJECT}.tmp

echo "DOCKER EXIT CODE: $exitCode"

date;

cd $HOME/data
if [ "${block}" == "1" ] ; then
  zip -r ${procc}_${block}_${SUBJECT}.zip $ID
else
  diff -rq ${ID}_bckp ${ID} | sed 's|Only in ||g' |sed 's|: |/|g' | grep -v ${ID}_bckp> $HOME/diffs_${procc}_${block}_${SUBJECT}.txt
  sudo rm -rf $HOME/data/${ID}_bckp;
  zip -r ${procc}_${block}_${SUBJECT}.zip `cat $HOME/diffs_${procc}_${block}_${SUBJECT}.txt`
  dx upload  --path $DNADirectory:/outputs/logs/ $HOME/diffs_${procc}_${block}_${SUBJECT}.txt
fi

dx upload -r --path $DNADirectory:/outputs/  $HOME/data/${procc}_${block}_${SUBJECT}.zip

cd $HOME

sudo rm -rf $HOME/diffs_${procc}_${block}_${SUBJECT}.txt;
sudo rm -rf $HOME/data/${procc}_${block}_${SUBJECT}.zip;
sudo rm -rf $HOME/data/${ID}

date;
