# DNANexus_scripts

Small set of scripts to process UKB subjects in the DNA Nexus platform

# Instructions

1. Create a subj.txt file with the IDs of the UKB subjects to process (in the form \$VISIT\$ID)
1. Run `script_process_all.sh`


If your DNA Nexus environment is properly initated, these subjects will be processed.
