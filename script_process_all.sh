#!/bin/bash
#
# Example: ./script_process_all.sh subj.txt
#    subj.txt is a txt file with a list of subjects to process
#    The format of the subject IDs in that file is $VISIT$ID (8 characters)
#

mkdir -p logs

nam=`date +"%m-%d-%y_%H-%M-%S.%s"`

./script_send_CPU_1.sh subj.txt &> logs/log_CPU_1_$nam.txt
echo
jID=`cat logs/log_CPU_1_$nam.txt | tail -n 1`
dx ssh $jID
echo

./script_send_generic.sh subj.txt 2 GPU &> logs/log_GPU_2_$nam.txt
echo
jID=`cat logs/log_GPU_2_$nam.txt | tail -n 1`
dx ssh $jID
echo

./script_send_generic.sh subj.txt 3 CPU &> logs/log_CPU_3_$nam.txt
echo
jID=`cat logs/log_CPU_3_$nam.txt | tail -n 1`
dx ssh $jID
echo

./script_send_generic.sh subj.txt 4 GPU &> logs/log_GPU_4_$nam.txt
echo
jID=`cat logs/log_GPU_4_$nam.txt | tail -n 1`
dx ssh $jID
echo

for elem in `cat subj.txt` ; do
  cat $elem > s.txt
  ./script_send_generic.sh s.txt 5 CPU &> logs/log_CPU_5_${nam}_$elem.txt
  echo
done
rm s.txt
