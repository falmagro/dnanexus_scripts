#!/bin/bash

num_cores="8";

rm -rf subj_*;

# Divide the list of subjects into sublists of 8
split -l ${num_cores} -a 6 -d subj.txt subj_${num_cores}_
split -l 4 -a 6 -d subj.txt subj_4_

for subj_file in `ls --color=none subj_${num_cores}_*` ; do
   ./script_send.sh ${subj_file} 1 CPU `cat ${subj_file} | wc -l` ;
   sleep 120;
done

for subj_file in `ls --color=none subj_${num_cores}_*` ; do
   ./script_send.sh ${subj_file} 2 GPU 1 ;
   sleep 120;
done

for subj_file in `ls --color=none subj_${num_cores}_*` ; do
   ./script_send.sh ${subj_file} 3 CPU `cat ${subj_file} | wc -l` ;
   sleep 120;
done

for subj_file in `ls --color=none subj_4_*` ; do
   ./script_send.sh ${subj_file} 4 GPU 1 ;
   sleep 120;
done

for subj_file in `ls --color=none subj_${num_cores}_*` ; do
   ./script_send.sh ${subj_file} 5 CPU `cat ${subj_file} | wc -l` ;
   sleep 120;
done

for subj_file in `ls --color=none subj_${num_cores}_*` ; do
   ./script_send.sh ${subj_file} 6 CPU `cat ${subj_file} | wc -l` ;
   sleep 120;
done
