#!/bin/bash
#
# Note: Change FBP_TESTS to the appropriate DNANexus directory
#

set -x

DNADirectory="FBP_TESTS"
block="$1";
procc="$2";
num_cores="$3"

if [ ${block} == "2" ] ; then
    dckr_name="fbp_gpu"
    image="${dckr_name}.tar.gz"
elif [ ${block} == "4" ] ; then
    dckr_name="fbp_gpu"
    image="${dckr_name}.tar.gz"
else
    dckr_name="fbp"
    image="${dckr_name}.tar"
fi

date;
dx download $DNADirectory:/dckr/${image} --overwrite
date;
sudo docker load --input $HOME/${image} ;
date;

sudo apt-get install moreutils --assume-yes
sudo apt-get install parallel --assume-yes

mkdir -p data ;

for SUBJECT in `cat $HOME/subjects.txt` ; do
   echo "./script_proc_subject.sh ${SUBJECT} ${block} ${procc} ${dckr_name} ${DNADirectory}"
done > job_subjects.txt

# Run jobs in parallel
cat job_subjects.txt | parallel --jobs ${num_cores}

t="$HOME/log_${procc}_${block}_*.txt"
dx upload    --path $DNADirectory:/outputs/logs/ $t

./killme
